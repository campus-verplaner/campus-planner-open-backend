# Campus Planner Open Backend

Since the https://campus-planer.com/ domain hasn't been renewed, the "Campus Planer" app by the [TH Köln](https://www.th-koeln.de) does not work anymore.
The goal of this project is to provide an open reverse-engineered backend to make the app usable again.

## Installation

To compile the project, you will need `shards` and the `crystal` compiler, as well as the libraries `bdwgc`, `openssl`, `pcre`, `pcre2` and `libc` (including their development headers).

If you meet these prequisites, simply run the following command:
`shards build --release --production`

The resulting server binary will be located in the `bin` directory. This binary is the only file needed to run the server (aside from the aforementioned libraries).

## Usage

### Starting the web service

To start the web service, just execute the server binary.
Optionally, you can set the `CPOB_SERVER_HOST` and `CPOB_SERVER_PORT` environment variables to set the application host (default: 0.0.0.0) and port (default: 3000).

`CPOB_SERVER_HOST="0.0.0.0" CPOB_SERVER_PORT="3000" ./campus_planner_open_backend`

### Connecting the android app

Since the android app tries to communicate with the domain https://campus-planer.com, it will not magically connect to the web service. To do that, the android app will be patched.

This will require `apktool` (for patching), `jdk` (for signing) and `adb` (for installing).
Also, you will need an https-enabled domain to reach your computer. For development purposes, you can use a service like `ngrok` (using command `ngrok http 3000`).

First, download the Campus Planer apk ([for example from apkpure](https://apkpure.com/de/campus-planer/de.mdxdave.campusplaner/download)). To follow this guide, rename the file to `CampusPlaner.apk`.

Decode the downloaded apk into a folder using apktool:
```bash
apktool d CampusPlaner.apk
```

Replace all occurrences of the string "api.campus-planer.com" with your local domain:
```bash
sed -i "s/api.campus-planer.com/your-domain.net/g" ./CampusPlaner/smali/de/mdxdave/campusplaner/utils/Utils.smali
```

Encode the apk again using apktool:
```bash
apktool b ./CampusPlaner
```

To sign the final apk, a keystore first needs to be created:
```bash
keytool -genkey -v -keystore my-release-key.keystore -alias keystore -keyalg RSA -keysize 4096 -validity 10000000
```
You will be asked for a password (just use "password", you will only need it once again). Also, you will be asked for various information (name, city, ...). Just click enter until the question `Is CN=Unknown, OU=Unknown, O=Unknown, L=Unknown, ST=Unknown, C=Unknown correct?` comes up - which you need to answer with `yes`. 

Finally, sign the resulting apk file:
```bash
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore CampusPlaner/dist/CampusPlaner.apk keystore
```
Here, you only need to type in your previously assigned password.

The final apk can now be installed on an android phone, but since it was signed with a self-signed certificate it must be installed through adb. The easiest way to do this is using adb-over-wifi. After you have connected to your device using adb, run: 
```bash
adb install ./CampusPlaner/dist/CampusPlaner.apk
```

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/campus-verplaner/campus_planner_open_backend/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [David Keller](https://gitlab.com/BlobCodes) - creator and maintainer
