struct Athena::Framework::Listeners::Error
  protected def log_exception(exception : Exception, & : -> String) : Nil
    if exception.is_a?(Athena::Framework::Exceptions::NotFound)
      Log.info { exception.message }
      return
    end

    previous_def(exception) { yield }
  end
end
