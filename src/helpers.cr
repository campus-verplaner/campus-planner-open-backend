JSON_HEADER = HTTP::Headers {
  "content-type" => MIME.from_extension(".json")
}

macro json(string)
  ATH::Response.new headers: JSON_HEADER, content: {{ string }}
end
