@[ARTA::Route(path: "/v3")]
class APIv3::Library < ATH::Controller
  @[ARTA::Post("/library/renew")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  @[ATHA::RequestParam("media_number")]
  def revew(username : String, password : String, media_number : String) : ATH::Response
    # status codes:
    # - non-1 => show toast with message in key "msg"
    # - 1 => success

    json <<-JS
      {
        "status": 1,
        "return_date": "19.09.2022"
      }
    JS
  end

  @[ARTA::Post("/library")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  def fetch(username : String, password : String) : ATH::Response
    # status codes:
    # - 0 => show toast with message in key "msg"
    # - 2 => credentials wrong / no access
    # - 1 => success

    # date -> loans -> renewed optional, not there has same meaning as 0
    # if there are elements in the key (date -> reminders), the app will crash with a NullpointerException, as it is not implemented
    json <<-JS
      {
        "status": 1,
        "counts": {
          "loans": 1,
          "orders": 1,
          "holds": 1,
          "reminders": 0
        },
        "data": {
          "loans": [
            {
              "title": "Buch1",
              "branch": "test",
              "number": "00123",
              "return_date": "09.07.2022",
              "loan_date": "09.07.2022",
              "renewable": true,
              "renewed": 2
            }
          ],
          "orders": [
            {
              "title": "Buch2",
              "branch": "test",
              "number": "00123",
              "order_date": "09.07.2022",
              "expected_return_date": "09.07.2022",
              "status": "idk"
            }
          ],
          "holds": [
            {
              "title": "Buch3",
              "branch": "test",
              "number": "00123",
              "hold_date": "09.07.2022",
              "hold_valid": "09.07.2022",
              "hold_position": "33"
            }
          ],
          "reminders": []
        }
      }
    JS
  end

  @[ARTA::Post("/library/checkLogin")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  def check_login(username : String, password : String) : ATH::Response
    # success => success: 1, error => success: non-1
    json <<-JS
      {
        "status": 1
      }
    JS
  end
end
