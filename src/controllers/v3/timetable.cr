@[ARTA::Route(path: "/v3")]
class APIv3::Timetable < ATH::Controller
  @[ARTA::Post("/timetable")]
  @[ATHA::RequestParam("subject")]
  @[ATHA::RequestParam("semester")]
  def fetch(subject : String, semester : String) : ATH::Response
    # id must be > 0
    # weekday from 1=mo to 5=fr

    json <<-JS
      {
        "status": 1,
        "data": [
          {
            "id": 10,
            "name": "Grundlagen der BWL",
            "room": "HFM",
            "roomname": "Hamburger Fischmarkt",
            "type": "Vorlesung",
            "starttime": "16:00",
            "endtime": "17:00",
            "weekday": 1,
            "lecturer": "Peter"
          }
        ]
      }
    JS
  end
end
