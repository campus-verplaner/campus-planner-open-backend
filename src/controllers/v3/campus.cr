@[ARTA::Route(path: "/v3")]
class APIv3::Campus < ATH::Controller
  @[ARTA::Get("/base/campus")]
  def index : ATH::Response
    json <<-JSON
      {
        "campus": [
          {
            "short": "gm",
            "name": {
              "en": "Gummersbach",
              "de": "Gummersbach"
            }
          },
          {
            "short": "lev",
            "name": {
              "en": "Leverkusen",
              "de": "Leverkusen"
            }
          },
          {
            "short": "iwz",
            "name": {
              "en": "Cologne-Deutz",
              "de": "K\\u00f6ln-Deutz"
            }
          },
          {
            "short": "gwz",
            "name": {
              "en": "Cologne-S\\u00fcdstadt",
              "de": "K\\u00f6ln-S\\u00fcdstadt"
            }
          }
        ]
      }
    JSON
  end
end
