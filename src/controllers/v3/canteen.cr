@[ARTA::Route(path: "/v3")]
class APIv3::Canteen < ATH::Controller
  @[ARTA::Get("/base/mensa")]
  def index : ATH::Response
    json <<-JS
      {
        "mensa": [
          {
            "short": "gm",
            "name": {
              "en": "Standort Gummersbach",
              "de": "Standort Gummersbach"
            }
          },
          {
            "short": "iwz",
            "name": {
              "en": "Standort Deutz",
              "de": "Standort Deutz"
            }
          },
          {
            "short": "gwz",
            "name": {
              "en": "Standort S\\u00fcdstadt",
              "de": "Standort S\\u00fcdstadt"
            }
          },
          {
            "short": "cafe",
            "name": {
              "en": "Standort Claudiusstra\\u00dfe",
              "de": "Standort Claudiusstra\\u00dfe"
            }
          }
        ]
      }
    JS
  end

  @[ARTA::Get("//mensa/mensa:{id}")]
  def fetch(id : String) : ATH::Response
    # sup fields unused
    json <<-JS
      {
        "mon": [
          {
            "name": "Kuchen",
            "img": "https://i.imgflip.com/1isbc7.jpg",
            "meal": {"name": "Leckerer Kuchen \\ud83d\\ude00", "sup": []},
            "descr": {"desc": "ER IST EINE L\\u00DCGE!!!", "sup": []},
            "price": [2.0, 3.0, 4.0]
          }
        ],
        "tue": [],
        "wed": [],
        "thu": [],
        "fri": []
      }
    JS
  end
end
