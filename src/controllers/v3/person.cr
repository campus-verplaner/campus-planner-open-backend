@[ARTA::Route(path: "/v3")]
class APIv3::Person < ATH::Controller
  @[ARTA::Get("/persons/campus:{campus_id}")]
  def index(campus_id : String) : ATH::Response
    json <<-JS
      {
        "persons": [
          {
            "name": "Test Person",
            "url": "person.test",
            "telefon": "+49 222-2222-2222",
            "email": "noreply@example.com"
          }
        ]
      }
    JS
  end

  @[ARTA::Get("/person/id:{person_id}")]
  def fetch(person_id : String) : ATH::Response
    # fields faculty and faculty_sub are UNUSED
    # field academic technically used, but could not find where

    json <<-JS
      {
        "detail": {
          "campus": "Campus Gummersbach",
          "email": "test.person@th-koeln.de",
          "phone": "+49 222-2222-2222",
          "hours": ["19:00-20:00, Raum 010"],
          "website": ["www.example.com"],
          "functions": ["Testsubjekt Aperture Science", "Hilfestellung App-Entwicklung"],
          "academic": "",
          "address": {
            "street": "Ubierring 48",
            "city": "50678 Köln"
          },
          "faculty": "",
          "faculty_sub": "",
          "room": "Raum 413",
          "image": "c3e1f2de033c9c5c257b8951fdc17f4abe394dc8.png"
        }
    }
    JS
  end
end
