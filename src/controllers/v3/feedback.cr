@[ARTA::Route(path: "/v3")]
class APIv3::Feedback < ATH::Controller
  @[ARTA::Post("/feedback")]
  @[ATHA::RequestParam("feedback_message")]
  @[ATHA::RequestParam("feedback_email")]
  @[ATHA::RequestParam("feedback_deviceinfo")]
  def create(feedback_message : String, feedback_email : String, feedback_deviceinfo : String) : ATH::Response
    ATH::Response.new
  end
end
