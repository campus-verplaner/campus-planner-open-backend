@[ARTA::Route(path: "/v3")]
class APIv3::Exam < ATH::Controller
  @[ARTA::Post("/api/")]
  @[ATHA::RequestParam("subject")]
  def fetch(subject : String) : ATH::Response
    # subject field unused
    # date/time fields displayed as-is

    json <<-JS
      {
        "status": 1,
        "data": [
          {
            "id": 1,
            "name": "AP2 Klausur",
            "subject": "",
            "semester": "2",
            "duration": 60,
            "examiner": "PT",
            "time": "19 Uhr",
            "date": "19.03.2022"
          }
        ]
      }
    JS
  end
end
