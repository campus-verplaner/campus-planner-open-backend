@[ARTA::Route(path: "/v3")]
class APIv3::Subject < ATH::Controller
  @[ARTA::Get("/base/subjects")]
  def index : ATH::Response
    json <<-JSON
      {
        "subjects": [
          {
            "short": "AI",
            "name": {
              "en": "General informatics (Bachelor)",
              "de": "Allgemeine Informatik (Bachelor)"
            }
          }
        ]
      }
    JSON
  end
end
