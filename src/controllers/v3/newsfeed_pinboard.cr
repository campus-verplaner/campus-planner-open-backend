@[ARTA::Route(path: "/v3")]
class APIv3::NewsfeedPinboard < ATH::Controller
  # All image paths are relative to https://th-koeln.de

  @[ARTA::Get("/base/pinboards")]
  def list_pinboards : ATH::Response
    json <<-JS
      {
        "pinboards": [
          {
            "short": "PB1",
            "name": {
              "en": "Test Pinboard",
              "de": "Test Pinboard"
            }
          }
        ]
      }
    JS
  end

  @[ARTA::Post("/pinboard")]
  @[ATHA::RequestParam("pinboard")]
  def fetch_pinboard(pinboard : String) : ATH::Response
    # Date in format yyyy-MM-dd hh:mm:ss

    json <<-JS
      {
        "status": 1,
        "data": [
          {
            "title": "Test",
            "url": "https://example.com",
            "date": "2022-08-19 20:02:50",
            "text": "Das ist ein test!"
          }
        ]
      }
    JS
  end

  @[ARTA::Get("/base/newsfeeds")]
  def list_newsfeeds : ATH::Response
    json <<-JS
      {
        "newsfeeds": [
          {
            "short": "TST",
            "name": {
              "en": "Test news feed",
              "de": "Test Newsfeed"
            }
          }
        ]
      }
    JS
  end

  @[ARTA::Post("/newsfeed")]
  @[ATHA::RequestParam("faculty")]
  def fetch_newsfeed(faculty : String) : ATH::Response
    json <<-JS
      {
        "status": 200,
        "data": [
          {
            "title": "Test",
            "url": "https://example.com",
            "image": "mam/bilder/hochschule/aktuelles/nachrichten/2022/fittosize_738_346_dd2b7fe5b1cc4ab52c16794dc5c74a5e_adobestock_520348987.jpeg",
            "text": "Das ist ein test!"
          }
        ]
      }
    JS
  end

  @[ARTA::Post("//newsreader")]
  @[ATHA::RequestParam("url")]
  def details(url : String) : ATH::Response
    json <<-JS
      {
        "status": 200,
        "title": "Test",
        "html": "Dieser text ist <u>unterstrichen</u>",
        "images": ["mam/bilder/hochschule/aktuelles/nachrichten/2022/fittosize_738_346_dd2b7fe5b1cc4ab52c16794dc5c74a5e_adobestock_520348987.jpeg"]
      }
    JS
  end
end
