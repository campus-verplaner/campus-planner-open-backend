require "base64"

@[ARTA::Route(path: "/v3")]
class APIv3::Ilias < ATH::Controller
  # Cannot be accessed using the android app
  #
  # @[ARTA::Post("/ilias/board/id:{id}")]
  # @[ATHA::RequestParam("username")]
  # @[ATHA::RequestParam("password")]
  # @[ATHA::RequestParam("page")]
  # @[ATHA::RequestParam("cache")]
  # def fetch_board(username : String, password : String, page : Int32, cache : String) : ATH::Response
  #   # status codes:
  #   # - (200, 204) => success
  #   # - (401, 403) => credentials wrong / no access
  #   # - 304 => use cache
  #   # - 110 => ilias course not shown, login required
  #   # - other => error
  #
  #   json <<-JS
  #     {
  #       "status": 200,
  #       "threads": [
  #         {
  #           "last": {
  #             "username": "mmmmmmmm",
  #             "date": "20.02.2003"
  #           },
  #           "id": 1,
  #           "boardId": 1,
  #           "name": "Test",
  #           "announcement": false,
  #           "unread": false,
  #           "username": "testuser",
  #           "posts": 20,
  #           "views": 20000
  #         }
  #       ],
  #       "pager": {
  #         "current": 1,
  #         "max": 1,
  #         "count": 10,
  #         "perpage": 10,
  #         "start": 1,
  #         "end": 10,
  #         "prev": false,
  #         "next": false
  #       }
  #     }
  #   JS
  # end

  @[ARTA::Post("/ilias/list/id:{id}")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  @[ATHA::RequestParam("cache")]
  def fetch_folder(id : String, username : String, password : String, cache : String) : ATH::Response
    # status codes:
    # - (200, 204) => success
    # - (401, 403) => credentials wrong / no access
    # - 304 => use cache
    # - 110 => ilias course not shown, login required
    # - other => error

    # types:
    # - "folder"
    # - "group"
    # - "url" (id unused)
    # - "category"
    # - "file"
    # - "board" (NOT SHOWN)

    # Dates displayed as-is

    json <<-JS
      {
        "status": 200,
        "items": [
          {
            "type": "folder",
            "id": 1,
            "name": "test folder",
            "new": true,
            "desc": "description"
          },
          {
            "type": "group",
            "id": 2,
            "name": "test group",
            "new": false,
            "desc": "description"
          },
          {
            "type": "url",
            "id": -1,
            "name": "test url",
            "url": "https://example.com"
          },
          {
            "type": "category",
            "id": 3,
            "name": "test category",
            "new": false,
            "desc": "description"
          },
          {
            "type": "file",
            "id": 4,
            "name": "test file",
            "properties": {
              "type": "jpg",
              "size": "2MB",
              "date": "20.02.2003",
              "new": false
            }
          },
          {
            "type": "board",
            "id": 5,
            "name": "test board",
            "desc": "description",
            "posts": {
              "count": 3,
              "new": 0,
              "last": {
                "author": "David",
                "date": "20.02.2003"
              }
            }
          }
        ]
      }
    JS
  end

  @[ARTA::Post("/ilias/list/goto:{goto}")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  @[ATHA::RequestParam("cache")]
  def goto_folder(goto : String, username : String, password : String, cache : String) : ATH::Response
    # status codes:
    # - (200, 204) => success
    # - (401, 403) => credentials wrong / no access
    # - 304 => use cache
    # - 110 => ilias course not shown, login required
    # - other => error

    # types:
    # - "folder"
    # - "group"
    # - "url" (id unused)
    # - "category"
    # - "file"
    # - "board" (NOT SHOWN)

    # Dates displayed as-is

    json <<-JS
      {
        "status": 200,
        "items": [
          {
            "type": "folder",
            "id": 1,
            "name": "test folder",
            "new": true,
            "desc": "description"
          },
          {
            "type": "group",
            "id": 2,
            "name": "test group",
            "new": false,
            "desc": "description"
          },
          {
            "type": "url",
            "id": -1,
            "name": "test url",
            "url": "https://example.com"
          },
          {
            "type": "category",
            "id": 3,
            "name": "test category",
            "new": false,
            "desc": "description"
          },
          {
            "type": "file",
            "id": 4,
            "name": "test file",
            "properties": {
              "type": "jpg",
              "size": "2MB",
              "date": "20.02.2003",
              "new": false
            }
          },
          {
            "type": "board",
            "id": 5,
            "name": "test board",
            "desc": "description",
            "posts": {
              "count": 3,
              "new": 0,
              "last": {
                "author": "David",
                "date": "20.02.2003"
              }
            }
          }
        ]
      }
    JS
  end

  @[ARTA::Get("/ilias/download/id:{id}")]
  def download(request : ATH::Request, id : Int32) : ATH::Response
    username = request.headers["X-Username"]?
    password = request.headers["X-Password"]?
    success = true
    if username && password
      begin
        username = Base64.decode_string(username)
        password = Base64.decode_string(password)
      rescue
        success = false
      end
    end
    raise ArgumentError.new unless username && password && success

    ATH::Response.new content: "This is a file you just downloaded!"
  end

  @[ARTA::Post("/ilias/message/id:{id}")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  def fetch_message(id : Int32, username : String, password : String) : ATH::Response
    # status codes:
    # - 200 => success
    # - 400 => bad request
    # - 401 => unauthorized
    # - 403 => forbidden
    # - 401.. => opens intent SettingsActivity

    # date is shown as-is if it has up to 2 spaces.
    # otherwise, the date is required to have exactly 3 spaces and
    # the second argument is parsed as a three-letter month name (e.g. Feb) and converted to a localized string with a dot at the end ("Mär" => "Mär." (de), ("Mar" (en)))

    json <<-JS
      {
        "status": 200,
        "message": {
          "message": "Das hier ist <b>meine</b> Nachricht!",
          "date": "3. Okt 2022, 20:05"
        }
      }
    JS
  end

  @[ARTA::Post("/ilias/messages")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  def index_messages(username : String, password : String) : ATH::Response
    # status codes:
    # - 200 => success
    # - 403 => forbidden
    # - else => opens intent AccountActivity

    # date is split at commas (e.g. "2022, Dec" => ["2022", "Dec"])
    # if the first element equals "Heute", the second element is displayed as the date
    # otherwise, the first element is shown as the date

    json <<-JS
      {
        "status": 200,
        "messages": [
          {
            "id": 1,
            "new": false,
            "subject": "Test message",
            "date": "20. Feb 2022, 20:05",
            "from": {
              "name": "maxmustermann",
              "image": "test.jpg"
            }
          }
        ]
      }
    JS
  end

  # Cannot be accessed using the android app
  #
  # @[ARTA::Post("/ilias/thread/boardId:{board_id}/threadId:{thread_id}")]
  # @[ATHA::RequestParam("username")]
  # @[ATHA::RequestParam("password")]
  # @[ATHA::RequestParam("page")]
  # @[ATHA::RequestParam("posts")]
  # @[ATHA::RequestParam("cache")]
  # def fetch_thread(username : String, password : String, board_id : String, thread_id : String, page : Int32, posts : String, cache : String) : ATH::Response
  #   # status codes:
  #   # - (200, 204) => success
  #   # - (401, 403) => credentials wrong / no access
  #   # - 304 => use cache
  #   # - 110 => ilias course not shown, login required
  #   # - other => error
  #   json <<-JS
  #     {
  #       "status": 200,
  #       "pager": {
  #         "current": 0,
  #         "max": 0,
  #         "count": 1,
  #         "perpage": 1,
  #         "start": 0,
  #         "end": 0,
  #         "prev": false,
  #         "next": false
  #       },
  #       "posts": [
  #         {
  #           "id": 0,
  #           "title": "",
  #           "date": "",
  #           "message": "",
  #           "unread": false,
  #           "edit": {
  #             "date": ""
  #           },
  #           "user": {
  #             "name": "",
  #             "username": ""
  #           }
  #         }
  #       ]
  #     }
  #   JS
  # end

  @[ARTA::Post("/ilias/checkLogin")]
  @[ATHA::RequestParam("username")]
  @[ATHA::RequestParam("password")]
  def check_login(username : String, password : String) : ATH::Response
    # success => success: 1, error => success: non-1
    json <<-JS
      {
        "status": 1
      }
    JS
  end
end
