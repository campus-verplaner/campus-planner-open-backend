require "athena"
require "./error_handling"
require "./helpers"
require "./controllers/v3"

# Server defaults
SERVER_HOST = ENV["CPOB_SERVER_HOST"]? || "0.0.0.0"
SERVER_PORT = (ENV["CPOB_SERVER_PORT"]? || 3000).to_i

ATH.run(SERVER_PORT, SERVER_HOST)
